import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Main {

    // Complete the migratoryBirds function below.
    static int migratoryBirds(List<Integer> arr) {
        int[] type={0,0,0,0,0};

        for (int i=0;i<arr.size();i++)
            type[arr.get(i)-1]=type[arr.get(i)-1]+1;


        int maxI=type.length-1;
        for (int i=type.length-1;i>=0;i--)
            if (type[i]>=type[maxI])
                maxI=i;

        return maxI+1;
    }
    public static void main(String[] args) throws IOException {


        List<Integer> arr = new ArrayList<>();
        arr.add(1);//0
        arr.add(2);//1
        arr.add(3);//2
        arr.add(4);//3
        arr.add(5);//4
        arr.add(4);//5
        arr.add(3);//6
        arr.add(2);//7
        arr.add(1);//8
        arr.add(3);//9
        arr.add(4);//10


        int result = migratoryBirds(arr);
        System.out.println(result);


    }
}
